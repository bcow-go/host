package host

type GenericMiddleware struct {
	InitFunc func(appCtx *Context)
}

func (c *GenericMiddleware) Init(appCtx *Context) {
	if appCtx == nil {
		panic("argument 'appCtx' cannot be nil")
	}

	c.InitFunc(appCtx)
}
