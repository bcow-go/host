package host

import (
	"context"
	"fmt"
	"log"

	"gitlab.bcowtech.de/bcow-go/config"
	"go.uber.org/fx"
)

type Starter struct {
	app           *fx.App
	context       *Context
	constructors  []interface{}
	configurators []interface{}

	// managers
	hostProviderManager *hostProviderManager
	configuratorManager *configuratorManager
	componentManager    *componentManager // Runable
	middlewareManager   *middlewareManager
}

func Startup(app interface{}, middlewares ...Middleware) *Starter {
	var (
		context             = makeContext(app)
		configuratorManager = newConfiguratorManager()
		middlewareManager   = newMiddlewareManager(context, middlewares...)
		hostProviderManager = newHostProviderManager(context)
		componentManager    = newComponent()
	)

	instance := Starter{
		context:             context,
		configuratorManager: configuratorManager,
		middlewareManager:   middlewareManager,
		hostProviderManager: hostProviderManager,
		componentManager:    componentManager,
	}
	instance.constructors =
		append(instance.constructors,
			[]interface{}{
				context.makeDefaultConstructor(AppHostFieldName),            // register Host instance
				context.makeDefaultConstructor(AppConfigFieldName),          // register Config instance
				context.makeDefaultConstructor(AppServiceProviderFieldName), // register ServiceProvider instance
				instance.registerHostInstance,                               // register Host interface
			}...)
	instance.configurators =
		append(instance.configurators,
			[]interface{}{
				instance.hostProviderManager.Init,               // init host
				middlewareManager.Load,                          // load all middlewares
				instance.hostProviderManager.PostLoadMiddleware, // config host after middlewares loaded
				instance.configureHostHook,                      // hook host for starting
			}...)

	return &instance
}

func (s *Starter) ConfigureConfiguration(configure ConfigureConfigurationFunc) *Starter {
	rv := s.context.FieldByName(AppConfigFieldName)

	s.beforeConfigureConfiguration()
	service := config.NewConfigurationService(rv.Interface())
	configure(service)
	// call AfterConfigureCompleted()
	s.afterConfigureConfiguration()
	return s
}

func (s *Starter) Configure(configure ConfigureFunc) *Starter {
	rv := s.context.FieldByName(AppConfigFieldName)
	config := rv.Interface()
	configure(config)

	return s
}

func (s *Starter) Start(ctx context.Context) error {
	s.initApp()
	return s.app.Start(ctx)
}

func (s *Starter) Stop(ctx context.Context) error {
	if s.app == nil {
		panic(fmt.Errorf("Starter did not call Start() yet"))
	}
	return s.app.Stop(ctx)
}

func (s *Starter) Run() {
	s.initApp()
	s.app.Run()
}

func (s *Starter) initApp() {
	s.configurators = append(s.configurators, s.configuratorManager.ToArray()...)

	s.app = fx.New(
		fx.Provide(s.constructors...),
		fx.Invoke(s.configurators...),
	)
}

func (s *Starter) beforeConfigureConfiguration() {
	s.context.initConfig()
}

func (s *Starter) afterConfigureConfiguration() {
	s.context.initApp()
	s.context.initHost()
	s.context.initServiceProvider()
}

func (s *Starter) registerHostInstance() Host {
	rv := s.context.FieldByName(AppHostFieldName)
	host := s.hostProviderManager.Emit(rv)
	if host == nil {
		panic(fmt.Errorf("specifed field Host (type %T) is not a Host interface", rv.Interface()))
	}
	return host
}

func (s *Starter) registerComponent() {
	var (
		rvApp = s.context.rv
	)
	if rvApp.IsValid() {
		count := rvApp.NumField()
		for i := 0; i < count; i++ {
			rvField := rvApp.Field(i)
			if !rvField.CanInterface() {
				continue
			}

			if !rvField.IsZero() {
				v := rvField.Interface()
				switch v.(type) {
				case Runable:
					s.componentManager.RegisterComponent(v.(Runable).Runner())
				case Runner:
					s.componentManager.RegisterComponent(v.(Runner))
				}
			}
		}
	}
}

func (s *Starter) configureHostHook(lc fx.Lifecycle, h Host) {
	lc.Append(
		fx.Hook{
			OnStart: func(ctx context.Context) error {
				s.registerComponent()
				go func() {
					s.componentManager.Start()
					log.Println("[bcow-go/host] Starting")
					h.Start(ctx)
				}()
				return nil
			},
			OnStop: func(ctx context.Context) error {
				s.componentManager.Stop()
				log.Println("[bcow-go/host] Shutdown")
				return h.Stop(ctx)
			},
		},
	)
}

func SetupHostProvider(starter *Starter, provider HostProvider) {
	if provider != nil {
		starter.hostProviderManager.Register(provider)
	}
}
