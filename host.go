package host

import (
	"context"
	"reflect"

	"gitlab.bcowtech.de/bcow-go/config"
)

const (
	AppHostFieldName            string = "Host"
	AppConfigFieldName          string = "Config"
	AppServiceProviderFieldName string = "ServiceProvider"
	ComponentInitMethodName     string = "Init"
)

type (
	Host interface {
		Start(ctx context.Context)
		Stop(ctx context.Context) error
	}

	Middleware interface {
		Init(app *Context)
	}

	HostProvider interface {
		Init(host Host, app *Context)
		PostLoadMiddleware(host Host, app *Context)
		Emit(rv reflect.Value) Host
	}

	Runner interface {
		Start()
		Stop()
	}

	Runable interface {
		Runner() Runner
	}

	ConfigureConfigurationFunc func(service *config.ConfigurationService)
	ConfigureFunc              func(config interface{})
)
