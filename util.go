package host

import "reflect"

func makeDefaultConstructor(rv reflect.Value) interface{} {
	if rv.IsValid() {
		impl := func(in []reflect.Value) []reflect.Value {
			return []reflect.Value{rv}
		}
		decl := reflect.FuncOf([]reflect.Type{}, []reflect.Type{rv.Type()}, false)

		return reflect.MakeFunc(decl, impl).Interface()
	}
	return nil
}
