package host

type configuratorManager struct {
	functions []interface{}
}

func newConfiguratorManager() *configuratorManager {
	return &configuratorManager{}
}

func (m *configuratorManager) Append(configurator ...interface{}) {
	m.functions = append(m.functions, configurator...)
}

func (m *configuratorManager) ToArray() []interface{} {
	return m.functions
}
