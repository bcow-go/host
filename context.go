package host

import (
	"fmt"
	"reflect"
)

type Context struct {
	pv reflect.Value
	rv reflect.Value
}

func makeContext(v interface{}) *Context {
	var rv reflect.Value
	switch v.(type) {
	case reflect.Value:
		rv = v.(reflect.Value)
	default:
		rv = reflect.ValueOf(v)
	}

	if !rv.IsValid() {
		panic(fmt.Errorf("specified argument 'v' is invalid"))
	}

	rv = reflect.Indirect(rv)

	return &Context{
		rv: rv,
		pv: rv.Addr(),
	}
}

func (ctx *Context) FieldByName(name string) reflect.Value {
	var rv = ctx.rv
	rvfield := rv.FieldByName(name)
	if rvfield.Kind() != reflect.Ptr {
		panic(fmt.Errorf("specified appContext field '%s' should be of type *%s", name, rvfield.Type().String()))
	}
	if rvfield.IsNil() {
		rvfield.Set(reflect.New(rvfield.Type().Elem()))
	}
	return rvfield
}

func (ctx *Context) HostField() reflect.Value {
	return ctx.FieldByName(AppHostFieldName)
}

func (ctx *Context) ConfigField() reflect.Value {
	return ctx.FieldByName(AppConfigFieldName)
}

func (ctx *Context) ServiceProviderField() reflect.Value {
	return ctx.FieldByName(AppServiceProviderFieldName)
}

func (ctx *Context) makeDefaultConstructor(name string) interface{} {
	var rv = ctx.FieldByName(name)
	return makeDefaultConstructor(rv)
}

func (ctx *Context) initApp() {
	var (
		pConfig = ctx.FieldByName(AppConfigFieldName)
		pApp    = ctx.pv
	)

	// get the instance Init() method
	fn := pApp.MethodByName(ComponentInitMethodName)
	if fn.IsValid() {
		if fn.Kind() != reflect.Func {
			panic(fmt.Errorf("invalid func %s(conf %s) within type %s", ComponentInitMethodName, pConfig.Type().String(), pApp.Type().Name()))
		}
		if fn.Type().NumIn() != 1 || fn.Type().NumOut() != 0 ||
			(fn.Type().In(0) != pConfig.Type()) {
			panic(fmt.Errorf("method type should be func %s.%s(conf %s)",
				pApp.Type().String(),
				ComponentInitMethodName,
				pConfig.Type().String()))
		}
		fn.Call([]reflect.Value{pConfig})
	}
}

func (ctx *Context) initServiceProvider() {
	var (
		pConfig          = ctx.FieldByName(AppConfigFieldName)
		pServiceProvider = ctx.FieldByName(AppServiceProviderFieldName)
	)

	// get the instance Init() method
	fn := pServiceProvider.MethodByName(ComponentInitMethodName)
	if fn.IsValid() {
		if fn.Kind() != reflect.Func {
			panic(fmt.Errorf("invalid func %s(...) within type %s", ComponentInitMethodName, pServiceProvider.Type().String()))
		}

		var args []reflect.Value
		if fn.Type().NumIn() > 0 {
			count := fn.Type().NumIn()
			for i := 0; i < count; i++ {
				paramType := fn.Type().In(i)
				switch paramType {
				case pConfig.Type():
					args = append(args, pConfig)
				case ctx.pv.Type():
					args = append(args, ctx.pv)
				default:
					panic(fmt.Errorf("unsupported type '%s' in %s.%s(...)",
						paramType.String(),
						pServiceProvider.Type().String(),
						ComponentInitMethodName))
				}
			}
		}
		fn.Call(args)
	}
}

func (ctx *Context) initHost() {
	var (
		pConfig = ctx.FieldByName(AppConfigFieldName)
		pHost   = ctx.FieldByName(AppHostFieldName)
	)

	// get the instance Init() method
	fn := pHost.MethodByName(ComponentInitMethodName)
	if fn.IsValid() {
		if fn.Kind() != reflect.Func {
			panic(fmt.Errorf("invalid func %s.%s(conf %s) within type %s", pHost.Type().String(), ComponentInitMethodName, pConfig.Type().String(), pHost.Type().Name()))
		}
		if fn.Type().NumIn() != 1 || fn.Type().NumOut() != 0 ||
			(fn.Type().In(0) != pConfig.Type()) {
			panic(fmt.Errorf("method type should be func %s.%s(conf %s)",
				pHost.Type().String(),
				ComponentInitMethodName,
				pConfig.Type().String()))
		}

		fn.Call([]reflect.Value{pConfig})
	}
}

func (ctx *Context) initConfig() {
	var (
		pConfig = ctx.FieldByName(AppConfigFieldName)
	)

	// get the instance Init() method
	fn := pConfig.MethodByName(ComponentInitMethodName)
	if fn.IsValid() {
		if fn.Kind() != reflect.Func {
			panic(fmt.Errorf("cannot find func %s() within type %s", ComponentInitMethodName, pConfig.Type().String()))
		}
		if fn.Type().NumIn() != 0 || fn.Type().NumOut() != 0 {
			panic(fmt.Errorf("method type should be func %s.%s()",
				pConfig.Type().String(),
				ComponentInitMethodName))
		}

		fn.Call([]reflect.Value(nil))
	}
}
