package host

type componentManager struct {
	components []Runner
}

func newComponent() *componentManager {
	return &componentManager{}
}

func (m *componentManager) Start() {
	if m.components != nil {
		for i := 0; i < len(m.components); i++ {
			component := m.components[i]
			component.Start()
		}
	}
}

func (m *componentManager) Stop() {
	if m.components != nil {
		for i := 0; i < len(m.components); i++ {
			component := m.components[i]
			component.Stop()
		}
	}
}

func (m *componentManager) RegisterComponent(component Runner) {
	if component != nil {
		m.components = append(m.components, component)
	}
}
