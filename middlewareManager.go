package host

type middlewareManager struct {
	appCtx      *Context
	middlewares []Middleware
}

func newMiddlewareManager(appCtx *Context, middlewares ...Middleware) *middlewareManager {
	if appCtx == nil {
		panic("argument 'appCtx' cannot be nil")
	}

	instance := middlewareManager{
		appCtx:      appCtx,
		middlewares: middlewares,
	}
	return &instance
}

func (c *middlewareManager) Load() {
	for _, v := range c.middlewares {
		v.Init(c.appCtx)
	}
}
