package host

import (
	"reflect"
)

type hostProviderManager struct {
	appCtx   *Context
	provider HostProvider
}

func newHostProviderManager(appCtx *Context) *hostProviderManager {
	if appCtx == nil {
		panic("argument 'appCtx' cannot be nil")
	}

	instance := &hostProviderManager{
		appCtx: appCtx,
	}
	return instance
}

func (p *hostProviderManager) Register(provider HostProvider) {
	p.provider = provider
}

func (p *hostProviderManager) Emit(rv reflect.Value) Host {
	v, ok := rv.Interface().(Host)
	if ok {
		return v
	}
	if p.provider != nil {
		return p.provider.Emit(rv)
	}
	return nil
}

func (p *hostProviderManager) Init(host Host) {
	if p.provider != nil {
		p.provider.Init(host, p.appCtx)
	}
}

func (p *hostProviderManager) PostLoadMiddleware(host Host) {
	if p.provider != nil {
		p.provider.PostLoadMiddleware(host, p.appCtx)
	}
}
