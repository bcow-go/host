package test

import (
	"gitlab.bcowtech.de/bcow-go/host"
)

type MockComponent struct {
}

func (c *MockComponent) Runner() host.Runner {
	return &MockComponentRunner{
		prefix: "MockComponent",
	}
}
